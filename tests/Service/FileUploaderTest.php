<?php

namespace App\Tests\Service;

use App\Service\FileUploader;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use PHPUnit\Framework\TestCase;

final class FileUploaderTest extends TestCase
{
    public function testCanBeCreated(): FileUploader
    {
        $fileUploader = new FileUploader(__DIR__);

        $this->assertInstanceOf(
            FileUploader::class,
            $fileUploader
        );

        return $fileUploader;
    }

    /**
     * @depends testCanBeCreated
     */
    public function testCanReturnFilename(fileUploader $fileUploader): string
    {
        file_put_contents(__DIR__.'/test.jpg', '');

        $uploadedFile = new UploadedFile(__DIR__.'/test.jpg', 'test.jpg', null, null, true);

        $filename = $fileUploader->upload($uploadedFile);

        $this->assertIsString($filename);

        return $filename;
    }

    /**
     * @depends testCanReturnFilename
     */
    public function testCanSaveFileToDirectory(string $filename): void
    {
        $this->assertFileExists(__DIR__.'/'.$filename);

        unlink(__DIR__.'/'.$filename);
    }
}