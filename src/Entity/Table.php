<?php

namespace App\Entity;

use App\Repository\TableRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TableRepository::class)
 * @ORM\Table(name="`table`")
 */
class Table
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     * @var int $id
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string $name
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=6)
     *
     * @var string $color
     */
    private $color;

    /**
     * @ORM\Column(type="smallint")
     *
     * @var int $height
     */
    private $height;

    /**
     * @ORM\Column(type="smallint")
     *
     * @var int $width
     */
    private $width;

    /**
     * @ORM\Column(type="smallint")
     *
     * @var int $length
     */
    private $length;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Chair")
     * @ORM\JoinColumn(name="chair_id", referencedColumnName="id")
     *
     * @var Chair $chair
     */
    private $chair;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getColor(): ?string
    {
        return $this->color;
    }

    /**
     * @param string $color
     *
     * @return $this
     */
    public function setColor(string $color): self
    {
        $this->color = $color;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getHeight(): ?int
    {
        return $this->height;
    }

    /**
     * @param int $height
     *
     * @return $this
     */
    public function setHeight(int $height): self
    {
        $this->height = $height;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getWidth(): ?int
    {
        return $this->width;
    }

    /**
     * @param int $width
     *
     * @return $this
     */
    public function setWidth(int $width): self
    {
        $this->width = $width;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getLength(): ?int
    {
        return $this->length;
    }

    /**
     * @param int $length
     *
     * @return $this
     */
    public function setLength(int $length): self
    {
        $this->length = $length;

        return $this;
    }

    /**
     * @return Chair
     */
    public function getChair(): Chair
    {
        return $this->chair;
    }

    /**
     * @param Chair $chair
     */
    public function setChair(Chair $chair): void
    {
        $this->chair = $chair;
    }
}
