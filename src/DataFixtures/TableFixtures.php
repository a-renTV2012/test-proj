<?php

namespace App\DataFixtures;

use App\Entity\Chair;
use App\Entity\Table;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class TableFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @param ObjectManager $manager
     *
     * @return void
     */
    public function load(ObjectManager $manager) : void
    {
        $table = new Table();
        $table->setName('Gray table');
        $table->setColor('e9e9e9');
        $table->setWidth('250');
        $table->setHeight('80');
        $table->setLength('100');
        $table->setChair($manager->getRepository(Chair::class)->findOneBy(['color' => 'e9e9e9']));

        $manager->persist($table);
        $manager->flush();
    }

    /**
     * @return array
     */
    public function getDependencies(): array
    {
        return [
            ChairFixtures::class,
        ];
    }
}