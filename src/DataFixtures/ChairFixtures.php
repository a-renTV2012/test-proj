<?php

namespace App\DataFixtures;

use App\Entity\Chair;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ChairFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     *
     * @return void
     */
    public function load(ObjectManager $manager) : void
    {
        $chair = new Chair();
        $chair->setName('Gray chair');
        $chair->setColor('e9e9e9');

        $manager->persist($chair);
        $manager->flush();
    }
}