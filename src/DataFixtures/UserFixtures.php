<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    /**
     * @var UserPasswordHasherInterface
     */
    private $ph;

    /**
     * @param UserPasswordHasherInterface $ph
     */
    public function __construct(UserPasswordHasherInterface $ph)
    {
        $this->ph = $ph;
    }

    /**
     * @param ObjectManager $manager
     *
     * @return void
     */
    public function load(ObjectManager $manager) : void
    {
        $admin = new User('admin', '123', ['ROLE_USER', 'ROLE_ADMIN'], $this->ph);
        $user = new User('user', 123, ['ROLE_USER'], $this->ph);

        $manager->persist($admin);
        $manager->persist($user);

        $manager->flush();
    }
}