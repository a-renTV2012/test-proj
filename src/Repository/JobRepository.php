<?php

namespace App\Repository;

use App\Entity\Job;
use App\Entity\Category;
use App\Entity\Affiliate;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\LazyCriteriaCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Collection;

class JobRepository extends EntityRepository
{
    /**
     * @param int $id
     *
     * @return Job
     */
    public function findActiveJob(int $id) : ?Job
    {
        $expr = Criteria::expr();
        $criteria = Criteria::create();

        $criteria->where($expr->gt('expiresAt', new \DateTime()));
        $criteria->andWhere($expr->eq('activated', true));
        $criteria->andWhere($expr->eq('id', $id));

        return $this->matching($criteria)[0];
    }

    /**
     * @param int|null $categoryId
     *
     * @return Collection|LazyCriteriaCollection
     */
    public function findActiveJobs(int $categoryId = null)
    {
        $expr = Criteria::expr();
        $criteria = Criteria::create();

        $criteria->where($expr->gt('expiresAt', new \DateTime()));
        $criteria->andWhere($expr->eq('activated', true));
        if ($categoryId)
            $criteria->andWhere($expr->eq('category', $categoryId));

        return $this->matching($criteria);
    }


    /**
     * @param Affiliate $affiliate
     *
     * @return Job[]
     */
    public function findActiveJobsForAffiliate(Affiliate $affiliate)
    {
        return $this->createQueryBuilder('j')
            ->leftJoin('j.category', 'c')
            ->leftJoin('c.affiliates', 'a')
            ->where('a.id = :affiliate')
            ->andWhere('j.expiresAt > :date')
            ->andWhere('j.activated = :activated')
            ->setParameter('affiliate', $affiliate)
            ->setParameter('date', new \DateTime())
            ->setParameter('activated', true)
            ->orderBy('j.expiresAt', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param Category $category
     *
     * @return AbstractQuery
     */
    public function getPaginatedActiveJobsByCategoryQuery(Category $category) : AbstractQuery
    {
        return $this->createQueryBuilder('j')
            ->where('j.category = :category')
            ->andWhere('j.expiresAt > :date')
            ->andWhere('j.activated = :activated')
            ->setParameter('category', $category)
            ->setParameter('date', new \DateTime())
            ->setParameter('activated', true)
            ->getQuery();
    }
}

?>