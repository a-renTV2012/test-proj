<?php

namespace App\Controller\EasyAdmin;

use App\Entity\Job;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\UrlField;

class JobCrudController extends AbstractCrudController
{
    /**
     * @return string
     */
    public static function getEntityFqcn(): string
    {
        return Job::class;
    }

    /**
     * @param string $pageName
     *
     * @return iterable
     */
    public function configureFields(string $pageName): iterable
    {
        return [
            ChoiceField::new('type')->setChoices([
                'Full-time' => 'full-time',
                'Part-time' => 'part-time',
                'Freelance' => 'freelance',
            ]),
            TextField::new('company'),
            ImageField::new('logo')->setUploadDir('public'.$this->getParameter('jobs_uploads_web_directory')),
            UrlField::new('url'),
            TextField::new('position'),
            TextField::new('location'),
            TextField::new('description'),
            TextField::new('howToApply'),
            BooleanField::new('public'),
            BooleanField::new('activated'),
            EmailField::new('email'),
            AssociationField::new('category'),
        ];
    }
}
