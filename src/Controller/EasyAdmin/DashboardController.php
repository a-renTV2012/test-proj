<?php

namespace App\Controller\EasyAdmin;

use App\Entity\Affiliate;
use App\Entity\Category;
use App\Entity\Chair;
use App\Entity\Job;
use App\Entity\Table;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/easy-admin", name="easy_admin")
     */
    public function index(): Response
    {
        $routeBuilder = $this->get(AdminUrlGenerator::class);

        return $this->redirect($routeBuilder->setController(JobCrudController::class)->generateUrl());
    }

    /**
     * @return Dashboard
     */
    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Jobeet');
    }

    /**
     * @return iterable
     */
    public function configureMenuItems(): iterable
    {
        return [
            MenuItem::linktoDashboard('Dashboard', 'fa fa-home'),

            MenuItem::section('Jobs'),
            MenuItem::linkToCrud('Categories', null, Category::class),
            MenuItem::linkToCrud('Jobs', null, Job::class),

            MenuItem::section('Affiliates'),
            MenuItem::linkToCrud('Affiliates', null, Affiliate::class),

            MenuItem::section('Furniture'),
            MenuItem::linkToCrud('Tables', null, Table::class),
            MenuItem::linkToCrud('Chairs', null, Chair::class),
        ];
    }
}
