<?php

namespace App\Controller\API;

use App\Entity\Affiliate;
use App\Entity\Job;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

class JobController extends AbstractFOSRestController
{
    /**
     * @Rest\Get("api/v1/{token}/jobs", name="api.job.list")
     *
     * @param string $token
     * @param EntityManagerInterface $em
     *
     * @return Response
     */
    public function getJobsAction(string $token, EntityManagerInterface $em) : Response
    {
        $affiliate = $em->getRepository((Affiliate::class))->findOneBy(['token' => $token]);

        if ($affiliate && $affiliate->isActive())
        {
            $jobs = $em->getRepository(Job::class)->findActiveJobsForAffiliate($affiliate);

            return $this->handleView($this->view($jobs, Response::HTTP_OK));
        }
        else
        {
            return $this->json('', Response::HTTP_OK);
        }
    }
}