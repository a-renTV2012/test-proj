<?php

namespace App\Controller\Admin;

use App\Entity\Affiliate;
use Swift_Message;
use Swift_Mailer;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AffiliateController extends AbstractController
{
    /**
     * Lists all affiliate entities.
     *
     * @Route("/admin/affiliates/{page}",
     *     name="admin.affiliate.list",
     *     methods="GET",
     *     defaults={"page": 1},
     *     requirements={"page" = "\d+"}
     * )
     *
     * @param EntityManagerInterface $em
     * @param PaginatorInterface $paginator
     * @param int $page
     *
     * @return Response
     */
    public function list(EntityManagerInterface $em, PaginatorInterface $paginator, int $page) : Response
    {
        $affiliates = $paginator->paginate(
            $em->getRepository(Affiliate::class)->createQueryBuilder('a'),
            $page,
            $this->getParameter('max_jobs_shown_in_category'),
            [
                PaginatorInterface::DEFAULT_SORT_FIELD_NAME => 'a.active',
                PaginatorInterface::DEFAULT_SORT_DIRECTION => 'ASC',
            ]
        );

        return $this->render('admin/affiliate/list.html.twig', [
            'affiliates' => $affiliates,
        ]);
    }

    /**
     * Activate affiliate.
     *
     * @Route("/admin/affiliate/{id}/activate",
     *     name="admin.affiliate.activate",
     *     methods="GET",
     *     requirements={"id" = "\d+"}
     * )
     *
     * @param EntityManagerInterface $em
     * @param Swift_Mailer $mailer
     * @param int $id
     *
     * @return Response
     */
    public function activate(EntityManagerInterface $em, Swift_Mailer $mailer, int $id) : Response
    {
        $affiliate = $em->find(Affiliate::class, $id);

        $affiliate->setActive(true);
        $em->flush();

        $message = (new Swift_Message())
            ->setSubject('Account activation')
            ->setTo('example@yandex.ru')
            ->setFrom('example@gmail.com')
            ->setBody(
                $this->renderView(
                    'emails/affiliate_activation.html.twig',
                    ['token' => $affiliate->getToken()]
                ),
                'text/html'
            );

        $mailer->send($message);

        return $this->redirectToRoute('admin.affiliate.list');
    }

    /**
     * Deactivate affiliate.
     *
     * @Route("/admin/affiliate/{id}/deactivate",
     *     name="admin.affiliate.deactivate",
     *     methods="GET",
     *     requirements={"id" = "\d+"}
     * )
     *
     * @param EntityManagerInterface $em
     * @param int $id
     *
     * @return Response
     */
    public function deactivate(EntityManagerInterface $em, int $id) : Response
    {
        $affiliate = $em->find(Affiliate::class, $id);

        $affiliate->setActive(false);
        $em->flush();

        return $this->redirectToRoute('admin.affiliate.list');
    }
}